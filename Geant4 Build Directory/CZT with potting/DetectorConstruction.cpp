#include "DetectorConstruction.hpp"
#include "geometry.hpp"

#include <G4Box.hh>
#include <G4Colour.hh>
#include <G4Element.hh>
#include <G4LogicalVolume.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4PolarizationManager.hh>
#include <G4PVPlacement.hh>
#include <G4PVReplica.hh>
#include <G4RotationMatrix.hh>
#include <G4ThreeVector.hh>
#include <G4UnitsTable.hh>
#include <G4VisAttributes.hh>
#include <G4Tubs.hh>
#include <cmath>


static G4RotationMatrix rotate90DegreeAroundZ(CLHEP::HepRotationZ(90*degree));


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Get the NIST material manager
  G4NistManager *nist = G4NistManager::Instance();

  // Option to switch on/off checking of volumes overlaps
  static const G4bool checkOverlaps = true;
  
  //
  // World
  //
  G4Material *worldMaterial = nist->FindOrBuildMaterial("G4_Galactic");

  G4Box *worldSolid = new G4Box(
    "World",          // name
    0.5 * WorldWidth, // x dimension
    0.5 * WorldWidth, // y dimension
    0.5 * WorldHeight // z dimension
    );

  G4LogicalVolume *worldLogical = new G4LogicalVolume(
    worldSolid,       // it's solid
    worldMaterial,    // it's material
    "World"           // it's name
    );

  G4VisAttributes *worldVisAttributes = new G4VisAttributes(G4Colour::Gray());
  worldVisAttributes->SetForceWireframe(true);
  worldLogical->SetVisAttributes(worldVisAttributes);

  G4VPhysicalVolume *worldPhysical = new G4PVPlacement(
    NULL,             // no rotation
    G4ThreeVector(),  // at (0, 0, 0)
    worldLogical,     // associated logical volume
    "World",          // name
    NULL,             // no mother volume
    false,            // no boolean operator
    0,                // copy number
    checkOverlaps
    );

  //
  // CZT detectors
  //

  G4Element* Cadmium = new G4Element(
	  "Cadmium",        // name
	  "Cd",             // symbol
	  48.,              // Z
	  112.411*g / mole    // <A>
	  );

  G4Element* Tellurium = new G4Element(
	  "Tellurium",      // name
	  "Te",             // symbol
	  52.,              // Z
	  126.60*g / mole     // <A>
	  );

  G4Element* Zinc = new G4Element(
	  "Zinc",           // name
	  "Zn",             // symbol
	  30.,              // Z
	  65.409*g / mole     // <A>
	  );

  G4Material* CdZnTe = new G4Material(
	  "CdZnTe",         // name
	  5.76*g / cm3,       // density
	  3                 // number of components
	  );

  // We assume Cd9 Zn1 Te10
  CdZnTe->AddElement(Cadmium, 9);
  CdZnTe->AddElement(Zinc, 1);
  CdZnTe->AddElement(Tellurium, 10);

  G4Box *cztSolid = new G4Box(
	  "CZT",            // name
	  0.5 * CZTWidth, // x dimension
	  0.5 * CZTWidth, // y dimension
	  0.5 * CZTThickness          // z dimension
	  );

  G4LogicalVolume *cztLogical = new G4LogicalVolume(
	  cztSolid,         // the solid
	  CdZnTe,           // material
	  "CZT"             // name
	  );

  G4VisAttributes *cztVisAttributes = new G4VisAttributes(G4Colour::Blue());
  cztLogical->SetVisAttributes(cztVisAttributes);

  G4VPhysicalVolume *cztPhysical = new G4PVPlacement(
	  NULL,             // no rotation
	  G4ThreeVector(0.5 * CZTWidth,
					0.5 * CZTWidth,
					0.5 * CZTThickness), // position
	  cztLogical,       // logical volume
	  "CZT",            // name
	  worldLogical,     // mother volume
	  false,            // no boolean operator
	  0,                // copy number
	  checkOverlaps
	  );

  //
  // Plotting Volume
  //

  G4Element* Carbon = new G4Element(
    "Carbon",             // name
    "C",                 // symbol
    6.,                 // Z
    12.011*g / mole    // <A>
    );

  G4Element* Hydrogen = new G4Element(
    "Hydrogen",           // name
    "H",                 // symbol
    1.,                 // Z
    1.008*g / mole     // <A>
    );

  G4Element* Nitrogen = new G4Element(
    "Nitrogen",            // name
    "N",                  // symbol
    7.,                  // Z
    14.007*g / mole     // <A>
    );

  G4Element* Oxygen = new G4Element(
    "Oxygen",              // name
    "O",                  // symbol
    8.,                  // Z
    15.999*g / mole     // <A>
    );

  G4Element* Phosphorus = new G4Element(
    "Phosphorus",          // name
    "P",                  // symbol
    15.,                 // Z
    30.974*g / mole     // <A>
    );

  G4Material* A1 = new G4Material(
    "A1",               // name
    1.23*g / cm3,       // density
    4                   // number of components
    );

  // C15 H10 N2 O2
  A1->AddElement(Carbon, 15);
  A1->AddElement(Hydrogen, 10);
  A1->AddElement(Nitrogen, 2);
  A1->AddElement(Oxygen, 2);

  G4Material* A2 = new G4Material(
    "A2",               // name
    1.18*g / cm3,       // density
    4                   // number of components
    );

  // C15 H10 N2 O2
  A2->AddElement(Carbon, 15);
  A2->AddElement(Hydrogen, 10);
  A2->AddElement(Nitrogen, 2);
  A2->AddElement(Oxygen, 2);  

  G4Material* A3 = new G4Material(
    "A3",               // name
    1.36*g / cm3,       // density
    4                   // number of components
    );

  // C15 H10 N2 O2
  A3->AddElement(Carbon, 15);
  A3->AddElement(Hydrogen, 10);
  A3->AddElement(Nitrogen, 2);
  A3->AddElement(Oxygen, 2);

  G4Material* A4 = new G4Material(
    "A4",               // name
    1.072*g / cm3,       // density
    4                   // number of components
    );

  // C6 H15 O4 P
  A4->AddElement(Carbon, 6);
  A4->AddElement(Hydrogen, 15);
  A4->AddElement(Oxygen, 4);
  A4->AddElement(Phosphorus, 1);

  G4Material* B1 = new G4Material(
    "B1",               // name
    1.2*g / cm3,       // density
    4                   // number of components
    );

  // C12 H19 N O2
  B1->AddElement(Carbon, 12);
  B1->AddElement(Hydrogen, 19);
  B1->AddElement(Nitrogen, 1);
  B1->AddElement(Oxygen, 2);

  G4Material* B2 = new G4Material(
    "B2",               // name
    0.899*g / cm3,       // density
    2                   // number of components
    );

  // C4 H6
  B2->AddElement(Carbon, 4);
  B2->AddElement(Hydrogen, 6);

  G4Material* A = new G4Material(
    "A",               // name
    1.21*g / cm3,       // density
    4                   // number of components
    );

  // Combination of the different ingrediants in A
  A->AddMaterial(A1, .25);
  A->AddMaterial(A2, .25);
  A->AddMaterial(A3, .25);
  A->AddMaterial(A4, .25);

  G4Material* B = new G4Material(
    "B",               // name
    1.05*g / cm3,       // density
    2                   // number of components
    );

  // Combination of the different ingrediants in B
  B->AddMaterial(B1, .5);
  B->AddMaterial(B2, .5);

  G4Material* potting = new G4Material(
    "potting",               // name
    1.08*g / cm3,       // density
    2                   // number of components
    );

  // The final potting mixture
  potting->AddMaterial(A, .16);
  potting->AddMaterial(B, .84);

  //  G4Material* potting = new G4Material(
  //   "potting",               // name
  //   1.08*g / cm3,       // density
  //   5                   // number of components
  //   );

  // // Total potting mixture
  // potting->AddElement(Carbon, 131);
  // potting->AddElement(Hydrogen, 170);
  // potting->AddElement(Nitrogen, 11);
  // potting->AddElement(Oxygen, 20);
  // potting->AddElement(Phosphorus, 1);


  G4Box *pottingSolid = new G4Box(
    "potting",                         // name
    0.5 * pottingWidth,               // x dimension
    0.5 * pottingWidth,              // y dimension
    0.5 * pottingThickness          // z dimension
    );

  G4LogicalVolume *pottingLogical = new G4LogicalVolume(
    pottingSolid,         // the solid
    potting,           // material
    "potting"             // name
    );

  G4VisAttributes *pottingVisAttributes = new G4VisAttributes(G4Colour::Green());
  pottingLogical->SetVisAttributes(pottingVisAttributes);

  G4VPhysicalVolume *pottingPhysical = new G4PVPlacement(
    NULL,             // no rotation
    G4ThreeVector(0.5*CZTWidth,
          0.5*CZTWidth,
          -0.15* CZTThickness), // position
    pottingLogical,       // logical volume
    "potting",            // name
    worldLogical,     // mother volume
    false,            // no boolean operator
    0,                // copy number
    checkOverlaps
    );

  // register logical volumes in PolarizationManager with zero polarization
  G4PolarizationManager *polarizationManager = G4PolarizationManager::GetInstance();
  polarizationManager->SetVolumePolarization(pottingLogical, G4ThreeVector(0.,0.,0.));
  polarizationManager->SetVolumePolarization(cztLogical, G4ThreeVector(0.,0.,0.));
  polarizationManager->SetVolumePolarization(worldLogical, G4ThreeVector(0.,0.,0.));
	
  // return the world volume
  return worldPhysical;
}
  
