#include "PhysicsList.hpp"

#include <G4EmParameters.hh>
#include <G4LossTableManager.hh>
#include <G4UAtomicDeexcitation.hh>
#include <G4PhysicsListHelper.hh>
#include <G4SystemOfUnits.hh>

#include <G4Geantino.hh>
#include <G4Gamma.hh>
#include <G4Electron.hh>
#include <G4Positron.hh>

#include <G4PhotoElectricEffect.hh>
#include <G4LivermorePolarizedPhotoElectricModel.hh>
#include <G4ComptonScattering.hh>
#include <G4LivermorePolarizedComptonModel.hh>
#include <G4GammaConversion.hh>
#include <G4LivermorePolarizedGammaConversionModel.hh>
#include <G4RayleighScattering.hh>
#include <G4LivermorePolarizedRayleighModel.hh>
#include <G4eMultipleScattering.hh>
#include <G4ePolarizedIonisation.hh>
#include <G4ePolarizedBremsstrahlung.hh>
#include <G4eplusPolarizedAnnihilation.hh>


const double PhysicsList::GammaRangeCut = 0.001*mm;
const double PhysicsList::ElectronRangeCut = 0.001*mm;
const double PhysicsList::PositronRangeCut = 0.001*mm;


void PhysicsList::ConstructParticle()
{
  // only QED

  // geantino, not sure if needed
  G4Geantino::GeantinoDefinition();

  // gamma
  G4Gamma::GammaDefinition();

  // leptons
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
}


void PhysicsList::ConstructProcess()
{
  //
  // Transportation
  //
  AddTransportation();

  //
  // Polarized Electromagnetic Processes
  //
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  // gamma
  G4ParticleDefinition* particle = G4Gamma::GammaDefinition();

  G4PhotoElectricEffect* PhotoElectricEffect = new G4PhotoElectricEffect();
  PhotoElectricEffect->SetEmModel(new G4LivermorePolarizedPhotoElectricModel);
  ph->RegisterProcess(PhotoElectricEffect, particle);

  G4ComptonScattering* ComptonScattering = new G4ComptonScattering();
  ComptonScattering->SetEmModel(new G4LivermorePolarizedComptonModel);
  ph->RegisterProcess(ComptonScattering, particle);

  G4GammaConversion* GammaConversion = new G4GammaConversion();
  GammaConversion->SetEmModel(new G4LivermorePolarizedGammaConversionModel);
  ph->RegisterProcess(GammaConversion, particle);

  G4RayleighScattering* RayleighScattering = new G4RayleighScattering();
  RayleighScattering->SetEmModel(new G4LivermorePolarizedRayleighModel);
  ph->RegisterProcess(RayleighScattering, particle);

  // electron
  particle = G4Electron::ElectronDefinition();

  ph->RegisterProcess(new G4eMultipleScattering, particle);
  ph->RegisterProcess(new G4ePolarizedIonisation, particle);
  ph->RegisterProcess(new G4ePolarizedBremsstrahlung, particle);

  // positron
  particle = G4Positron::PositronDefinition();

  ph->RegisterProcess(new G4eMultipleScattering, particle);
  ph->RegisterProcess(new G4ePolarizedIonisation, particle);
  ph->RegisterProcess(new G4ePolarizedBremsstrahlung, particle);
  ph->RegisterProcess(new G4eplusPolarizedAnnihilation, particle);

  //
  // Enable secondary emission (fluorescence, Auger electrons, ...)
  //
  G4EmParameters* emOptions = G4EmParameters::Instance();
  emOptions->SetDefaults();
  emOptions->SetFluo(true); // To activate deexcitation processes and fluorescence
  emOptions->SetAuger(true); // To activate Auger effect if deexcitation is activated
//  emOptions->SetPIXE(true); // To activate Particle Induced X-Ray Emission (PIXE)
//  emOptions.SetDeexcitationActiveRegion("World", true, true, true);
  emOptions->ActivateAngularGeneratorForIonisation(true);

  G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
}


void PhysicsList::SetCuts()
{
  SetCutValue(GammaRangeCut, "gamma");
  SetCutValue(ElectronRangeCut, "e-");
  SetCutValue(PositronRangeCut, "e+");
}
