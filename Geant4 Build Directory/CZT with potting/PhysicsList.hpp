#ifndef ACTSIM_PHYSICSLIST_HPP_INCLUDED
#define ACTSIM_PHYSICSLIST_HPP_INCLUDED

#include <G4VUserPhysicsList.hh>


class PhysicsList : public G4VUserPhysicsList {
public:
  PhysicsList() : G4VUserPhysicsList() {}
  ~PhysicsList() {}

  void ConstructParticle();
  void ConstructProcess();
  void SetCuts();

private:
  static const double GammaRangeCut;
  static const double ElectronRangeCut;
  static const double PositronRangeCut;
};

#endif // ACTSIM_PHYSICSLIST_HPP_INCLUDED
