if (NOT TCLAP_FOUND)

  message(STATUS "  Looking for tclap/CmdLine.h")
  find_path(TCLAP_INCLUDE_DIR tclap/CmdLine.h
    PATHS ${TCLAP_INCLUDE_PATH}
    )
  
  if (NOT ${TCLAP_INCLUDE_DIR} MATCHES ".*NOTFOUND$")
    message(STATUS "  Looking for tclap/CmdLine.h -- found")
    set(TCLAP_FOUND TRUE CACHE BOOL "TCLAP headers were found" FORCE)

  else (NOT ${TCLAP_INCLUDE_DIR} MATCHES ".*NOTFOUND$")
    
    message(STATUS "  Looking for tclap/CmdLine.h -- not found")
    
  endif (NOT ${TCLAP_INCLUDE_DIR} MATCHES ".*NOTFOUND$")
endif (NOT TCLAP_FOUND)
