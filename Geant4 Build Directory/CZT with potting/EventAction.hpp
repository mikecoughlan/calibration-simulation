#ifndef ACTSIM_EVENTACTION_HPP_INCLUDED
#define ACTSIM_EVENTACTION_HPP_INCLUDED

#include <G4UserEventAction.hh>

class Storage;


class EventAction : public G4UserEventAction {
public:
  EventAction(Storage &storage)
    : G4UserEventAction(), count_(0), storage_(storage)
  {}

  ~EventAction() {}

  void BeginOfEventAction(const G4Event *event);
  void EndOfEventAction(const G4Event*);

private:
  int count_;
  Storage &storage_;
};

#endif // ACTSIM_EVENTACTION_HPP_INCLUDED
