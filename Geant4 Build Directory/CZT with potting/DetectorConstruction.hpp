#ifndef ACTSIM_DETECTORCONSTRUCTION_HPP_INCLUDED
#define ACTSIM_DETECTORCONSTRUCTION_HPP_INCLUDED

#include <G4VUserDetectorConstruction.hh>

class G4VPhysicalVolume;

class DetectorConstruction : public G4VUserDetectorConstruction {
public:
  DetectorConstruction() : G4VUserDetectorConstruction() {}
  ~DetectorConstruction() {}

  G4VPhysicalVolume* Construct();
};

#endif // ACTSIM_DETECTORCONSTRUCTION_HPP_INCLUDED
