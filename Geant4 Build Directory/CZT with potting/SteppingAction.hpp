#ifndef ACTSIM_STEPPINGACTION_HPP_INCLUDED
#define ACTSIM_STEPPINGACTION_HPP_INCLUDED

#include <G4UserSteppingAction.hh>

class Storage;


class SteppingAction : public G4UserSteppingAction {
public:
  SteppingAction(Storage &storage) : storage_(storage) {}
  ~SteppingAction() {}

  void UserSteppingAction(const G4Step *step);

private:
  Storage &storage_;
};

#endif // ACTSIM_STEPPINGACTION_HPP_INCLUDED
