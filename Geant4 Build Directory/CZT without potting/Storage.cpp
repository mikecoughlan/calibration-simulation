#include "Storage.hpp"

#include <globals.hh>
#include <G4SystemOfUnits.hh>

#include <TBranch.h>
#include <TFile.h>
#include <TTree.h>

#include <map>


typedef std::map<G4String, UShort_t> NameMap_t;


namespace {

  NameMap_t create_particle_name_map()
  {
    NameMap_t particle_name_map;

    particle_name_map["gamma"] = 0;
    particle_name_map["e-"] = 1;
    particle_name_map["e+"] = 2;

    return particle_name_map;
  }

  NameMap_t create_volume_name_map()
  {
    NameMap_t volume_name_map;

    volume_name_map["World"] = 0;
    volume_name_map["CZT"] = 1;
    // THis is where I add the new volume number

    return volume_name_map;
  }

  NameMap_t create_interaction_name_map()
  {
    NameMap_t interaction_name_map;

    interaction_name_map["Transportation"] = 0;
    interaction_name_map["compt"] = 1;
    interaction_name_map["phot"] = 2;
    interaction_name_map["conv"] = 3;
    interaction_name_map["Rayl"] = 4;
    interaction_name_map["pol-eIoni"] = 5;
    interaction_name_map["pol-eBrem"] = 6;
    interaction_name_map["msc"] = 7;
    interaction_name_map["pol-annihil"] = 8;
    
    return interaction_name_map;
  }

}


Storage::Storage()
  : good_(false), compressed_(false), tree_(NULL)
{}


Storage::Storage(const G4String &filename)
  : good_(false), compressed_(false), tree_(NULL),
    particle_(100), volume_(100), interaction_(100), x1_(100), y1_(100),
    z1_(100), eKin_(100), x2_(100), y2_(100), z2_(100), eDep_(100)
{
  TFile *file = new TFile(filename, "RECREATE");
  if (!file->IsOpen()) {
    delete file;
    return;
  }

  tree_ = new TTree("events", "Events and Tracks");

  if (!tree_) {
    delete file;
    return;
  }
  
  good_ = true;
  
  tree_->Branch("ePrim", &ePrim_, "ePrim/D");
  tree_->Branch("xPrim", &xPrim_, "xPrim/D");
  tree_->Branch("yPrim", &yPrim_, "yPrim/D");
  tree_->Branch("zPrim", &zPrim_, "zPrim/D");
  tree_->Branch("nParticle", &nParticle_, "nParticle/I");
  particleBranch_ = tree_->Branch("particle", &particle_[0], "particle[nParticle]/s");
  volumeBranch_ = tree_->Branch("volume", &volume_[0], "volume[nParticle]/s");
  interactionBranch_ = tree_->Branch("interaction", &interaction_[0], "interaction[nParticle]/s");
  x1Branch_ = tree_->Branch("x1", &x1_[0], "x1[nParticle]/D");
  y1Branch_ = tree_->Branch("y1", &y1_[0], "y1[nParticle]/D");
  z1Branch_ = tree_->Branch("z1", &z1_[0], "z1[nParticle]/D");
  eKinBranch_ = tree_->Branch("eKin", &eKin_[0], "eKin[nParticle]/D");
  x2Branch_ = tree_->Branch("x2", &x2_[0], "x2[nParticle]/D");
  y2Branch_ = tree_->Branch("y2", &y2_[0], "y2[nParticle]/D");
  z2Branch_ = tree_->Branch("z2", &z2_[0], "z2[nParticle]/D");
  eDepBranch_ = tree_->Branch("eDep", &eDep_[0], "eDep[nParticle]/D");
}


Storage::~Storage()
{
  if (tree_) {
    tree_->Write();
    tree_->GetCurrentFile()->Close();
  }
}


void Storage::NewEvent(G4double energy, const G4ThreeVector &position)
{
  if (!tree_) return;
  
  ePrim_ = energy/keV;
  xPrim_ = position.x()/mm;
  yPrim_ = position.y()/mm;
  zPrim_ = position.z()/mm;

  nParticle_ = 0;
  particle_.clear();
  volume_.clear();
  interaction_.clear();
  x1_.clear();
  y1_.clear();
  z1_.clear();
  eKin_.clear();
  x2_.clear();
  y2_.clear();
  z2_.clear();
  eDep_.clear();
}


void Storage::AddTrack(const G4String &particle, const G4String &volume,
                       const G4String &interaction,
                       const G4ThreeVector &start, G4double eKin,
                       const G4ThreeVector &end, G4double eDep)
{
  static const NameMap_t particle_name_map = create_particle_name_map();
  static const NameMap_t volume_name_map = create_volume_name_map();
  static const NameMap_t interaction_name_map = create_interaction_name_map();

  if (!tree_) return;

  NameMap_t::const_iterator volumeIter = volume_name_map.find(volume);
  UShort_t iVolume = 0xFFFF;
  if (volumeIter != volume_name_map.end()) {
    iVolume = volumeIter->second;
  } else {
    G4cout << "Encountered unknown volume '" << volume << "'" << G4endl;
  }

  if (compressed_ && iVolume < 2) return;
  
  NameMap_t::const_iterator interactionIter = interaction_name_map.find(interaction);
  UShort_t iInteraction = 0xFFFF;
  if (interactionIter != interaction_name_map.end()) {
    iInteraction = interactionIter->second;
  } else {
    G4cout << "Encountered unknown interaction '" << interaction << "'" << G4endl;
  }

  if (compressed_ && iInteraction < 1) return;

  NameMap_t::const_iterator particleIter = particle_name_map.find(particle);
  UShort_t iParticle = 0xFFFF;
  if (particleIter != particle_name_map.end()) {
    iParticle = particleIter->second;
  } else {
    G4cout << "Encountered unknown particle '" << particle << "'" << G4endl;
  }

  particle_.push_back(iParticle);
  volume_.push_back(iVolume);
  interaction_.push_back(iInteraction);
  x1_.push_back(start.x()/mm);
  y1_.push_back(start.y()/mm);
  z1_.push_back(start.z()/mm);
  eKin_.push_back(eKin/keV);
  x2_.push_back(end.x()/mm);
  y2_.push_back(end.y()/mm);
  z2_.push_back(end.z()/mm);
  eDep_.push_back(eDep/keV);
}


void Storage::FinishEvent()
{
  if (!tree_) return;
  
  ResetBranchAddress(particleBranch_, particle_);
  ResetBranchAddress(volumeBranch_, volume_);
  ResetBranchAddress(interactionBranch_, interaction_);
  ResetBranchAddress(x1Branch_, x1_);
  ResetBranchAddress(y1Branch_, y1_);
  ResetBranchAddress(z1Branch_, z1_);
  ResetBranchAddress(eKinBranch_, eKin_);
  ResetBranchAddress(x2Branch_, x2_);
  ResetBranchAddress(y2Branch_, y2_);
  ResetBranchAddress(z2Branch_, z2_);
  ResetBranchAddress(eDepBranch_, eDep_);

  nParticle_ = particle_.size();
  
  tree_->Fill();
}


template <typename T>
void Storage::ResetBranchAddress(TBranch *branch, std::vector<T> &v)
{
  if (!branch) return;
  if (v.empty()) return;

  void *newAddress = &v[0];
  void *currentAddress = branch->GetAddress();
  if (currentAddress != newAddress) branch->SetAddress(newAddress);
}
