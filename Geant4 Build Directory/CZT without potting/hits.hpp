#ifndef ACTSIM_HITS_HPP_INCLUDED
#define ACTSIM_HITS_HPP_INCLUDED

struct CZTHit {
  double x;
  double y;
  double z;
  double energy;
};


struct SiHit {
  double x;
  double y;
  double z;
  int plane;
  double energy;
};

#endif // ACTSIM_HITS_HPP_INCLUDED
