#include "PrimaryGeneratorAction.hpp"
#include "geometry.hpp"

#include <G4Event.hh>
#include <G4ParticleGun.hh>
#include <G4ParticleTable.hh>
#include <G4ThreeVector.hh>
#include <iostream>
#include <random>
#include <functional>
#include <ctime>
#include <Randomize.hh>

using namespace std;

class G4ParticleDefinition;



PrimaryGeneratorAction::PrimaryGeneratorAction(const G4String& particleName,
                                             G4double q)
  : G4VUserPrimaryGeneratorAction(),
    particleGun(new G4ParticleGun(1)) // one primary particle per event
{

    
  // default particle kinematic
  G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition *particle = particleTable->FindParticle(particleName);
  particleGun->SetParticleDefinition(particle);
  // particleGun->SetParticleEnergy(energy);
  particleGun->SetParticlePolarization(G4ThreeVector(0., 0., 0.));
  particleGun->SetParticleMomentumDirection(G4ThreeVector(0., 0., 1.));
}


PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete particleGun;
}


void PrimaryGeneratorAction::GeneratePrimaries(G4Event *event)
{
	particleGun->SetParticlePosition(
		G4ThreeVector(0.5 * CZTWidth, 0.5 * CZTWidth, -10 * CZTThickness)
    );
  particleGun->GeneratePrimaryVertex(event);

    double energy;
    static uniform_real_distribution<double> u(0,235.972);
    static default_random_engine e (time(0));
    vector <double> ret;
    
    // // for (int i = 0; i < event; ++i)  {
    // //     ret.push_back(u(e));
        double N = u(e);
        
        if (N <= 37.7)
            energy = 40.1186;
        else if (N <= 66.11)
            energy = 121.7817;
        else if (N > 66.11 && N <= 92.7)
            energy = 344.2785;
        else if (N > 92.7 && N <= 113.55)
            energy = 1408.013;
        else if (N > 113.55 && N <= 134.35)
            energy = 39.5229;
        else if (N > 134.35 && N <= 148.85)
            energy = 964.079;
        else if (N > 148.85 && N <= 162.26)
            energy = 1112.076;
        else if (N > 162.26 && N <= 175.26)
            energy = 6.395;
        else if (N > 175.26 && N <= 188.23)
            energy = 778.9045;
        else if (N > 188.23 && N <= 200.01)
            energy = 45.4777;
        else if (N > 200.01 && N <= 210.14)
            energy = 1085.837;
        else if (N > 210.14 && N <= 217.69)
            energy = 244.6974;
        else if (N > 217.69 && N <= 221.933)
            energy = 867.38;
        else if (N > 221.933 && N <= 225.053)
            energy = 443.965;
        else if (N > 225.053 && N <= 228.093)
            energy = 46.6977;
        else if (N > 228.093 && N <= 230.331)
            energy = 411.1165;
        else if (N > 230.331 && N <= 232.061)
            energy = 1089.737;
        else if (N > 232.061 && N <= 233.694)
            energy = 1299.142;
        else if (N > 233.694 && N <= 235.11)
            energy = 1212.948;
        else
            energy = 367.7891;

    particleGun -> SetParticleEnergy(energy*keV);

}
