#include "SteppingAction.hpp"
#include "Storage.hpp"

#include <G4ParticleDefinition.hh>
#include <G4Step.hh>
#include <G4StepPoint.hh>
#include <G4Track.hh>
#include <G4VPhysicalVolume.hh>
#include <G4VProcess.hh>

#include <sstream>


void SteppingAction::UserSteppingAction(const G4Step *step)
{
  const G4Track *track = step->GetTrack();

  const G4StepPoint *startPoint = step->GetPreStepPoint();
  const G4StepPoint *endPoint = step->GetPostStepPoint();

  G4String particleName =
    track->GetDynamicParticle()->GetDefinition()->GetParticleName();

  G4VPhysicalVolume *volume = track->GetVolume();
  G4String volumeName = volume->GetName();

  G4String processName = endPoint->GetProcessDefinedStep()->GetProcessName();

  storage_.AddTrack(particleName, volumeName, processName,
                    startPoint->GetPosition(), startPoint->GetKineticEnergy(),
                    endPoint->GetPosition(), step->GetTotalEnergyDeposit());
}
