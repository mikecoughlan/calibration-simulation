#ifndef ACTSIM_GEOMETRY_HPP_INCLUDED
#define ACTSIM_GEOMETRY_HPP_INCLUDED

#ifndef BUILDING_ANALYZE
#include <globals.hh>
#else
typedef double G4double;
static const double mm = 1;
static const double cm = 10 * mm;
#endif

#include <cmath>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>

static const G4double CZTWidth = 2 * cm;
static const G4double CZTThickness = 2 * mm;

static const G4double WorldHeight = 100 * cm;
static const G4double WorldWidth = 100 * cm;


#endif // ACTSIM_GEOMETRY_HPP_INCLUDED
