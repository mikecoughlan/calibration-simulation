#include "DetectorConstruction.hpp"
#include "geometry.hpp"

#include <G4Box.hh>
#include <G4Colour.hh>
#include <G4Element.hh>
#include <G4LogicalVolume.hh>
#include <G4Material.hh>
#include <G4NistManager.hh>
#include <G4PolarizationManager.hh>
#include <G4PVPlacement.hh>
#include <G4PVReplica.hh>
#include <G4RotationMatrix.hh>
#include <G4ThreeVector.hh>
#include <G4UnitsTable.hh>
#include <G4VisAttributes.hh>
#include <G4Tubs.hh>
#include <cmath>


static G4RotationMatrix rotate90DegreeAroundZ(CLHEP::HepRotationZ(90*degree));


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Get the NIST material manager
  G4NistManager *nist = G4NistManager::Instance();

  // Option to switch on/off checking of volumes overlaps
  static const G4bool checkOverlaps = true;
  
  //
  // World
  //
  G4Material *worldMaterial = nist->FindOrBuildMaterial("G4_Galactic");

  G4Box *worldSolid = new G4Box(
    "World",          // name
    0.5 * WorldWidth, // x dimension
    0.5 * WorldWidth, // y dimension
    0.5 * WorldHeight // z dimension
    );

  G4LogicalVolume *worldLogical = new G4LogicalVolume(
    worldSolid,       // it's solid
    worldMaterial,    // it's material
    "World"           // it's name
    );

  G4VisAttributes *worldVisAttributes = new G4VisAttributes(G4Colour::Gray());
  worldVisAttributes->SetForceWireframe(true);
  worldLogical->SetVisAttributes(worldVisAttributes);

  G4VPhysicalVolume *worldPhysical = new G4PVPlacement(
    NULL,             // no rotation
    G4ThreeVector(),  // at (0, 0, 0)
    worldLogical,     // associated logical volume
    "World",          // name
    NULL,             // no mother volume
    false,            // no boolean operator
    0,                // copy number
    checkOverlaps
    );

  //
  // CZT detectors
  //

  G4Element* Cadmium = new G4Element(
	  "Cadmium",        // name
	  "Cd",             // symbol
	  48.,              // Z
	  112.411*g / mole    // <A>
	  );

  G4Element* Tellurium = new G4Element(
	  "Tellurium",      // name
	  "Te",             // symbol
	  52.,              // Z
	  126.60*g / mole     // <A>
	  );

  G4Element* Zinc = new G4Element(
	  "Zinc",           // name
	  "Zn",             // symbol
	  30.,              // Z
	  65.409*g / mole     // <A>
	  );

  G4Material* CdZnTe = new G4Material(
	  "CdZnTe",         // name
	  5.76*g / cm3,       // density
	  3                 // number of components
	  );

  // We assume Cd9 Zn1 Te10
  CdZnTe->AddElement(Cadmium, 9);
  CdZnTe->AddElement(Zinc, 1);
  CdZnTe->AddElement(Tellurium, 10);

  G4Box *cztSolid = new G4Box(
	  "CZT",            // name
	  0.5 * CZTWidth, // x dimension
	  0.5 * CZTWidth, // y dimension
	  0.5 * CZTThickness          // z dimension
	  );

  G4LogicalVolume *cztLogical = new G4LogicalVolume(
	  cztSolid,         // the solid
	  CdZnTe,           // material
	  "CZT"             // name
	  );

  G4VisAttributes *cztVisAttributes = new G4VisAttributes(G4Colour::Blue());
  cztLogical->SetVisAttributes(cztVisAttributes);

  G4VPhysicalVolume *cztPhysical = new G4PVPlacement(
	  NULL,             // no rotation
	  G4ThreeVector(0.5 * CZTWidth,
					0.5 * CZTWidth,
					0.5 * CZTThickness), // position
	  cztLogical,       // logical volume
	  "CZT",            // name
	  worldLogical,     // mother volume
	  false,            // no boolean operator
	  0,                // copy number
	  checkOverlaps
	  );

  // register logical volumes in PolarizationManager with zero polarization
  G4PolarizationManager *polarizationManager = G4PolarizationManager::GetInstance();
  polarizationManager->SetVolumePolarization(cztLogical, G4ThreeVector(0.,0.,0.));
  polarizationManager->SetVolumePolarization(worldLogical, G4ThreeVector(0.,0.,0.));
	
  // return the world volume
  return worldPhysical;
}
  
