#include "DetectorConstruction.hpp"
#include "EventAction.hpp"
#include "PhysicsList.hpp"
#include "PrimaryGeneratorAction.hpp"
#include "SteppingAction.hpp"
#include "Storage.hpp"

#include <G4RunManager.hh>
#include <Randomize.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>

#include <G4UImanager.hh>
#include <G4UIExecutive.hh>

#ifdef G4VIS_USE
#include <G4VisExecutive.hh>
#endif

#include "tclap/CmdLine.h"

#include <string>


int main(int argc, char **argv)
{
  bool interactive = false;
  bool compressed = false;
  std::string filename = "";
  int nEvents = 0;
  double energy = 0.;
  int polarizedQ = 0;

  try{
    using namespace TCLAP;
    using std::string;

    CmdLine cmd("ACT Geant4 simulation program", ' ', "trunk");

    ValueArg<int> nEventsArg("n", "nevents", "Number of events", false, nEvents,
                             "events", cmd);
    // ValueArg<double> energyArg("e", "energy", "Photon energy", true, energy,
    //                            "keV", cmd);
    ValueArg<int> polarizedQArg("q", "polq", "Stokes parameter Q", false,
                                polarizedQ, "0, 1, -1", cmd);
    ValueArg<string> filenameArg("o", "output", "Output file name", false,
                                 filename, "file", cmd);
    SwitchArg interactiveArg("i", "interactive",
                             "Start GUI instead of batch simulation");
    cmd.add(interactiveArg);
    SwitchArg compressedArg("c", "compressed", "Reduce output file size");
    cmd.add(compressedArg);

    cmd.parse(argc, argv);

    nEvents = nEventsArg.getValue();
    // energy = energyArg.getValue();
    polarizedQ = polarizedQArg.getValue();
    filename = filenameArg.getValue();
    interactive = interactiveArg.getValue();
    compressed = compressedArg.getValue();
  } catch (TCLAP::ArgException &e) {
    std::cerr << e.error() << " for argument " << e.argId() << std::endl;
    return 1;
  }

  if (!interactive) {
    if (nEvents == 0) {
      std::cerr << "In non-interactive mode you must enter a number of events"
                << std::endl;
      return 1;
    }

    if (filename.empty()) {
      std::cerr << "In non-interactive mode you must enter a file name"
                << std::endl;
      return 1;
    }
  }

#ifndef G4VIS_USE
  if (interactive) {
    std::cerr << "Recompile with G4VIS_USE enabled in order to use interactive "
              << "mode" << std::endl;
    return 1;
  }
#endif
  
  Storage *storage;
  if (filename.empty()) storage = new Storage;
  else {
    storage = new Storage(filename);
    if (!storage->good()) {
      std::cerr << "Failed to open output file '" << filename << "'" << std::endl;
      delete storage;
      return 1;
    }
    storage->setCompressed(compressed);
  }
          
  G4RunManager *runManager = new G4RunManager;

  // Choose the Random engine
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  
  runManager->SetUserInitialization(new DetectorConstruction);
  runManager->SetUserInitialization(new PhysicsList);

  // runManager->SetUserAction(new PrimaryGeneratorAction("gamma", energy*keV, polarizedQ));
  runManager->SetUserAction(new PrimaryGeneratorAction("gamma", polarizedQ));
  runManager->SetUserAction(new EventAction(*storage));
  runManager->SetUserAction(new SteppingAction(*storage));

  runManager->Initialize();

#ifdef G4VIS_USE
  G4VisManager *visManager = NULL;
#endif
  if (interactive) {
#ifdef G4VIS_USE
    // visualization manager
    visManager = new G4VisExecutive;
    // G4VisExecutive can take a verbosity argument - see /vis/verbose guidance.
    // G4VisManager* visManager = new G4VisExecutive("Quiet");
    visManager->Initialize();
#endif
    
    G4UIExecutive * ui = new G4UIExecutive(argc, argv);
    G4UImanager::GetUIpointer()->ApplyCommand("/control/execute init_vis.mac"); 
    ui->SessionStart();
    
    delete ui;
  } else {   // !interactive
    runManager->BeamOn(nEvents);
  }
  
  delete runManager;
   
#ifdef G4VIS_USE
  if (visManager) delete visManager;
#endif

  delete storage;
  
  return 0;
}
