# File FindROOT.cmake
#
# $Author: kislat $
# $Date: 2013-03-26 15:27:31 -0500 (Di, 26 Mär 2013) $
# $Id: FindROOT.cmake 101571 2013-03-26 20:27:31Z kislat $
# $Revision: 101571 $
#
# This file is part of treetool
# Copyright (C) 2010 Fabian Kislat   <fabian.kislat@desy.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

message(STATUS "Searching for ROOT")
find_program(ROOTCONFIG root-config PATHS ${ROOT_BIN_DIR} $ENV{ROOTSYS}/bin)
find_program(ROOTCINT rootcint PATHS ${ROOT_BIN_DIR} $ENV{ROOTSYS}/bin)
  
if(${ROOTCONFIG} MATCHES ".*NOTFOUND$" OR ${ROOTCINT} MATCHES ".*NOTFOUND$")

  message(STATUS "Searching for ROOT -- not found")
  set(ROOT_FOUND FALSE CACHE BOOL "ROOT was found" FORCE)

else(${ROOTCONFIG} MATCHES ".*NOTFOUND$" OR ${ROOTCINT} MATCHES ".*NOTFOUND$")

  execute_process(COMMAND ${ROOTCONFIG} --version
    OUTPUT_VARIABLE ROOT_VERSION_STRING OUTPUT_STRIP_TRAILING_WHITESPACE)
  string(REGEX MATCH "^[0-9].[0-9][0-9]" ROOT_VERSION ${ROOT_VERSION_STRING})

  execute_process(COMMAND ${ROOTCONFIG} --libdir
    OUTPUT_VARIABLE ROOT_LIBRARY_DIR_TMP OUTPUT_STRIP_TRAILING_WHITESPACE)
  set(ROOT_LIBRARY_DIR ${ROOT_LIBRARY_DIR_TMP} CACHE PATH "Path to the ROOT libraries")
# TODO: try to find a way to replace this with output from root-config
  foreach(root_lib Core Tree RIO)
    find_library(${root_lib}_path lib${root_lib}.so ${ROOT_LIBRARY_DIR})
    set(ROOT_LIBRARIES ${ROOT_LIBRARIES} ${${root_lib}_path})
  endforeach(root_lib Core Cint Tree Thread RIO MathCore Hist Matrix Net Graf Graf3d Gpad TreePlayer)

  execute_process(COMMAND ${ROOTCONFIG} --incdir
    OUTPUT_VARIABLE ROOT_INCLUDE_DIR_TMP OUTPUT_STRIP_TRAILING_WHITESPACE)
  set(ROOT_INCLUDE_DIR ${ROOT_INCLUDE_DIR_TMP} CACHE PATH "Path to the ROOT header files")

  message(STATUS "Searching for ROOT -- found version ${ROOT_VERSION}")

  set(ROOT_FOUND TRUE CACHE BOOL "ROOT was found" FORCE)

  if (NOT ROOT_VERSION LESS 5.26)
    message(STATUS "ROOT supports read-ahead")
    add_definitions(-DROOT_HAS_READAHEAD)
  endif (NOT ROOT_VERSION LESS 5.26)

endif(${ROOTCONFIG} MATCHES ".*NOTFOUND$" OR ${ROOTCINT} MATCHES ".*NOTFOUND$")



macro(require_root target)

  include_directories(${ROOT_INCLUDE_DIR})
  target_link_libraries(${target} ${ROOT_LIBRARIES} dl)

endmacro(require_root target)
