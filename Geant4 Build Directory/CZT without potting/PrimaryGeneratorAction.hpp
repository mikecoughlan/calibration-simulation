#ifndef ACTSIM_PRIMARYACTIONGENERATOR_HPP_INCLUDED
#define ACTSIM_PRIMARYACTIONGENERATOR_HPP_INCLUDED

#include <G4VUserPrimaryGeneratorAction.hh>
#include <G4ThreeVector.hh>

class G4ParticleGun;
class G4Event;


class PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction {
public:
  PrimaryGeneratorAction(const G4String& particleName,
                         G4double q);
  ~PrimaryGeneratorAction();

  void GeneratePrimaries(G4Event *event);

private:
  G4ParticleGun *particleGun;
};

#endif // ACTSIM_PRIMARYACTIONGENERATOR_HPP_INCLUDED
