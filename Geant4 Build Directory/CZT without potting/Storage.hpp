#ifndef ACTSIM_STORAGE_HPP_INCLUDED
#define ACTSIM_STORAGE_HPP_INCLUDED

#include <globals.hh>
#include <G4String.hh>
#include <G4ThreeVector.hh>

#include <Rtypes.h>

#include <vector>

class TBranch;
class TTree;


class Storage {
public:
  Storage();
  Storage(const G4String &filename);
  ~Storage();

  void setCompressed(bool compressed = true) { compressed_ = compressed; }
  
  void NewEvent(G4double energy, const G4ThreeVector &position);
  void AddTrack(const G4String &particle, const G4String &volume,
                const G4String &interaction,
                const G4ThreeVector &start, G4double eKin,
                const G4ThreeVector &end, G4double eDep);
  void FinishEvent();

  bool good() const { return good_; }
  
private:
  bool good_;
  bool compressed_;
  TTree *tree_;
  Double_t ePrim_;
  Double_t xPrim_;
  Double_t yPrim_;
  Double_t zPrim_;
  Int_t nParticle_;
  std::vector<UShort_t> particle_;
  TBranch *particleBranch_;
  std::vector<UShort_t> volume_;
  TBranch *volumeBranch_;
  std::vector<UShort_t> interaction_;
  TBranch *interactionBranch_;
  std::vector<Double_t> x1_;
  TBranch *x1Branch_;
  std::vector<Double_t> y1_;
  TBranch *y1Branch_;
  std::vector<Double_t> z1_;
  TBranch *z1Branch_;
  std::vector<Double_t> eKin_;
  TBranch *eKinBranch_;
  std::vector<Double_t> x2_;
  TBranch *x2Branch_;
  std::vector<Double_t> y2_;
  TBranch *y2Branch_;
  std::vector<Double_t> z2_;
  TBranch *z2Branch_;
  std::vector<Double_t> eDep_;
  TBranch *eDepBranch_;

  template <typename T>
  void ResetBranchAddress(TBranch *branch, std::vector<T> &v);
};

#endif // ACTSIM_STORAGE_HPP_INCLUDED
