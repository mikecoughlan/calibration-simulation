#include "EventAction.hpp"
#include "Storage.hpp"

#include <G4Event.hh>
#include <G4PrimaryParticle.hh>
#include <G4PrimaryVertex.hh>


void EventAction::BeginOfEventAction(const G4Event *event)
{
  const G4PrimaryVertex *vertex = event->GetPrimaryVertex();
  const G4PrimaryParticle *particle = vertex->GetPrimary();

  storage_.NewEvent(particle->GetKineticEnergy(), vertex->GetPosition());
}


void EventAction::EndOfEventAction(const G4Event*)
{
  storage_.FinishEvent();
  if (++count_ % 10000 == 0) G4cout << "Event " << count_ << " done" << G4endl;
}
